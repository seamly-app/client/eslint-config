# Seamly Eslint Config Changelog

## [Unreleased]

## 3.2.0 (5 September 2024)

### Changed

- Updated dependencies (minor and patch updates)

## 3.1.0 (2 February 2024)

### Changed

- Updated eslint-plugins
- Updated @seamly/prettier-config to 3.1.0
- Updated @seamly/babel-preset to 1.1.0

## 3.0.2 (2 November 2023)

### Changed

- Updated eslint-plugins

## 3.0.1 (5 October 2023)

### Changed

- Updated eslint to 8.50.x
- Updated eslint-plugins
- Updated @seamly/prettier-config to 3.0.3

## 3.0.0 (7 August 2023)

### Changed

- Updated eslint to 8.46.x
- Updated prettier to 3.0.x
- Updated eslint-plugins
- Updated @seamly/prettier-config to 3.0.0

## 2.3.0 (7 November 2022)

### Changed

- Updated eslint to 8.27.x
- Updated prettier to 2.7.x
- Updated eslint-plugins
- Updated @seamly/prettier-config to 2.2.0

## 2.2.0 (10 June 2022)

### Changed

- Updated eslint to 8.17.0

## 2.1.0 (2 November 2021)

### Changed

- Updated eslint to 8.17.0
- Disabled `parserOptions.requireConfigFile` to allow linting of files without an associated babel-config file
- Provided default babel-preset of `@seamly/babel-preset` for linting rules

## 2.0.0 (29 October 2021)

### Changed

- Added `filenames/match-regex` to enforce kebab-casing for filenames
- Updated eslint to 8.x
- Updated eslint-plugins and eslint-configs
- Updated prettier to 2.x
- Updated @seamly/prettier-config to 2.0.0
- Replaced babel-eslint with @babel/eslint-parser

## 1.0.2 (25 January 2021)

### Changed

- Updated @seamly/prettier-config to 1.0.2
- Updated webpack preset to match web-ui settings
- Fixed eslint dependencies to match default web-ui style

## 1.0.1 (22 January 2021)

### Changed

- Downgraded `eslint` devDependency and peerDependency from 2.x to 1.x
