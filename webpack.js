module.exports = {
  extends: ['./index.js', './rules/webpack.js'],
  settings: {
    'import/resolver': 'webpack',
  },
}
