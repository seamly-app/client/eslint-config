const prettierConfig = require('@seamly/prettier-config')

module.exports = {
  plugins: ['prettier'],
  extends: ['prettier'],

  rules: {
    'prettier/prettier': ['error', prettierConfig],
    'arrow-body-style': 'off',
    'prefer-arrow-callback': 'off',
  },
}
