module.exports = {
  globals: {
    __webpack_public_path__: true,
  },

  rules: {
    'import/no-webpack-loader-syntax': 0,
  },
}
