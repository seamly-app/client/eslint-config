module.exports = {
  env: {
    browser: true,
  },

  parser: '@babel/eslint-parser',
  parserOptions: {
    // allow linting on files that do not have a babel config file associated to them
    // since we use babel config within the webpack config
    requireConfigFile: false,
    babelOptions: {
      presets: ['@seamly/babel-preset'],
    },
  },

  plugins: ['filenames'],

  globals: {
    __BUILD__: true,
    __DEV__: true,
    process: true,
    CONFIG: true,
  },

  extends: ['airbnb-base'],

  rules: {
    'default-case': 0,
    'default-param-last': 0,
    'filenames/match-regex': [2, '^([a-z][a-z0-9]*)((\\.|-)[a-z0-9]+)*$'],
    'func-names': 0,
    'global-require': 0,
    'import/extensions': ['error', { js: 'never', json: 'always' }],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'import/prefer-default-export': 0,
    'no-case-declarations': 0,
    'no-console': 0,
    'no-else-return': 0,
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'no-prototype-builtins': 0,
    'no-restricted-properties': 0,
    'no-underscore-dangle': [
      'error',
      { allow: ['_waitingActions', '_instances'] },
    ],
    'no-unused-vars': ['warn', { varsIgnorePattern: '^h$' }],
    'no-var': 'error',
    'no-param-reassign': ['error', { props: false }],
    'prefer-arrow-callback': 0,
    'prefer-const': 'error',
  },
  ignorePatterns: ['.eslintrc.js'],
}
