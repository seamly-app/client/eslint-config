module.exports = {
  plugins: ['react-hooks'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },

  extends: ['plugin:react/recommended'],

  rules: {
    'react/jsx-no-bind': [2, { allowArrowFunctions: true }],
    'react/jsx-no-duplicate-props': 2,
    'react/self-closing-comp': 2,
    'react/prefer-es6-class': 2,
    'react/no-string-refs': 2,
    'react/require-render-return': 2,
    'react/no-find-dom-node': 2,
    'react/no-is-mounted': 2,
    'react/jsx-no-comment-textnodes': 2,
    'react/jsx-curly-spacing': 2,
    'react/jsx-no-undef': 2,
    'react/jsx-uses-react': 2,
    'react/jsx-uses-vars': 2,
    'react/react-in-jsx-scope': 0,
    'react/prop-types': 0,
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'error',
  },
}
