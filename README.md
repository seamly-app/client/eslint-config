## Seamly eslint-config

This is the default eslint configuration for `@seamly/*` implementations.

## Installation

```
yarn add -D @seamly/eslint-config
// or
npm install -D @seamly/eslint-config
```

## Usage

This package can be used as `@seamly/eslint-config` following the Eslint documentation on [Extending configuration files](https://eslint.org/docs/user-guide/configuring#extending-configuration-files).

#### Editing `.eslintrc.js`

```
module.exports = {
  ...
  extends: [
    '@seamly/eslint-config'
  ]
  ...
}
```

### Usage with React 
For react-specific defaults, the package exports a react-ruleset which implements `eslint-plugin-react` and `eslint-plugin-react-hooks`, including:
- `react-hooks/rules-of-hooks`
- `react-hooks/exhaustive-deps`

This ruleset implements on top of the default `@seamly/eslint-config`.

```
module.exports = {
  ...
  extends: [
    '@seamly/eslint-config/react'
  ]
  ...
}
```

### Usage in a webpack environment
For webpack-specific defaults, the package exports a webpack-ruleset which implements `eslint-import-resolver-webpack`.

This ruleset implements on top of the default `@seamly/eslint-config`.

```
module.exports = {
  ...
  extends: [
    '@seamly/eslint-config/webpack'
  ]
  ...
}
```
